﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeasarCipher
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            string my_input = Console.ReadLine();

            string my_alphabet = "abcdefghijklmnopqrstuvwxyz";
            string my_shiftedAlphabet = "defghijklmnopqrstuvwxyzabc";
            string my_output = "";

            // for ([the assignment]; [the condition]; [the action on every iteration])
            for (int my_count = 0; my_count < my_input.Length; my_count++)
            {
                char my_currentLetter = my_input[my_count];

                for (int my_currentIndex = 0; my_currentIndex < my_alphabet.Length; my_currentIndex++)
                {
                    if(my_currentLetter == my_alphabet[my_currentIndex])
                    {
                        // my_theOutput = my_theOutput + my_shiftedAlphabet[my_currentIndex];
                        my_output += my_shiftedAlphabet[my_currentIndex];
                        break;
                    }
                }
            }
            Console.WriteLine(my_output);

            Console.WriteLine("Press any key to stop the program");
            Console.ReadKey();
        }
    }
}
