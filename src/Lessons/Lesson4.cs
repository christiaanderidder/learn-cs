﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessons
{
    #region Class

    public class Person
    {
        public int age;
        public string firstName;
        public string lastName;

        public string IntroduceYourself()
        {
            return "Hello, I am " + firstName + " " + lastName + ". I am " + age + "years old.";
        }
    }

    #endregion

    #region Enum

    public enum TrafficLightColor
    {
        Red,
        Orange,
        Green
    }

    #endregion

    class Lesson4
    {
        public static void Run()
        {
            #region Enum

            TrafficLightColor myLight = TrafficLightColor.Green;
            //myLight = "hello!";                  // Bad. myLight is 'TrafficLightColor' not 'string'
            //myLight = 1234;                      // Bad. myLight is 'TrafficLightColor' not 'int'
            myLight = TrafficLightColor.Red;       // That's better.

            #endregion

            #region Class

            Person steve = new Person();
            steve.firstName = "Stephen";
            steve.lastName = "Hawking";
            steve.age = 72;

            string introduction = steve.IntroduceYourself();
            Console.WriteLine(introduction);

            #endregion
        }
    }
}
