﻿using System;
using System.Text;

namespace Lessons
{
    class Program
    {
        static void Main(string[] args)
        {
            // Make sure we can print cyrillic letters.
            Console.OutputEncoding = Encoding.Unicode;

            //Lesson1.Run();
            //Lesson1Excersize.Run();
            //Lesson2.Run();
            //Lesson3.Run();
            //Lesson4.Run();
            Experiments.Run();
        }
    }
}
