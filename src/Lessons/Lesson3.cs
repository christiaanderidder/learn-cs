﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessons
{
    class Lesson3
    {
        public static void Run()
        {
            #region 1 Functions/Methods

            // Functions or Methods
            // A function is a part of the code that encapsulates/isolates a specific task.

            // Programming languages often have a default set of functions that make our lives a bit easier.
            // For example:
            Console.WriteLine("Test 123");
            Console.Clear();
            string input = Console.ReadLine();
            double myNumber = Math.Round(5.9);

            // What is important here is that we do not have to worry about what happens there, we
            // can just assume it will read or write to the console, or round a number.

            // When we use a function, we say we "Call" the function.
            // When we write
            double roundedNumber = Math.Round(1.9);
            // 1. The code will see we are trying to call a function: "Math.Round()"
            // 2. The computer will jump into the function and run the code top to bottom
            //    here the magic of rounding the number happens.
            // 3. When the code inside the function has been executed the computer will
            //    jump back to the place where it has been called: "double roundedNumber = Math.Round(1.9);"
            // 4. If the function returns any data, this is what the computer will see at the
            //    place where the function had been called, in this example: "double roundedNumber = 2";           

            #endregion

            #region 2 Custom Functions

            string endResult = RepeatAWordMultipleTimes("car", 5);
            string endResult2 = RepeatAWordMultipleTimes("hi", 10);

            Console.WriteLine(endResult);

            #endregion

            #region 3 Why use Functions?

            // 1. Split a program in multiple small sub-steps, with their own responsibility
            // 2. Reuse these steps instead of copying them or rewriting them
            // 3. Keep the global scope clean, this means that temporary variables
            //    are not accessible by code outside of the function.
            // 4. Only expose the input and output, but not the inner workings (black box).
            //    For example: We don't need to know that Console.WriteLine("Hello") adds a newline
            //    character to our string, writes it to a buffered output stream, and flushes the stream
            //    to the output window.

            #endregion
        }

        #region 2 Custom Functions

        // Up until now we have just used build-in functions, of course we can write our own as well.
        void SayHi()
        {
            Console.WriteLine("hi");
            // The return type is a special type called "void", this means we don't have to return anything.
        }

        string DoSomethingAndReturnString()
        {
            // The return type is "string", so we have to return a string.
            // To do so, we use the special keyword "return". This will make the computer
            // jump out of the function and replace the function call with the string we returned.
            return "hi"; 
        }

        void DoSomethingWithANumberAndSomeText(int number, string text, bool someBoolean, double myDouble)
        {
            // The return type once again is "void", but now we have parameters.
            // You can have as many parameters as you like, from any type.
            // In this case we have a number and some text. 

            // We can now use these parameters inside the function:
            Console.WriteLine(number);
            Console.WriteLine(text);
        }

        int MultiplyTwoIntegers(int firstNumber, int secondNumber)
        {
            // This time we have a return type, and two parameters.
            // All of them are of the type int.
            // Now we can do the following: 
            return firstNumber * secondNumber;
        }
        
        static string RepeatAWordMultipleTimes(string stringThatContainsAWord, int numberOfTimes)
        {
            // This is a more complex example of a function, which will actually do something.
            // It has two parameters, one of type string and one of type int.
            // It's also a good example of naming, because the name of the function immediately
            // explains what it does. 

            string stringToStoreTheRepeatingWords = "";

            for (int count = 0; count < numberOfTimes; count++)
            {
                // Add a space between the words, of course we don't need to do this for the first word.
                // For example, let's take the word car and repeat it 5 times.
                // So instead of:
                // " car car car car car" <- space in front of every item
                // we want:
                // "car car car car car" <- no space in front of the first item
                if (count > 0)
                {
                    stringToStoreTheRepeatingWords += ",";
                }

                // Add the word
                stringToStoreTheRepeatingWords += stringThatContainsAWord;

                /*
                int num = 5; // num -> 5
                num += 10; // num -> 15

                string text = "Hi"; // text -> Hi
                text += " there";  // text -> Hi there
                */
            }
            return stringToStoreTheRepeatingWords;
        }

        #endregion

        #region 4 Building Blocks

        // Building blocks:

        // Return Type:         string
        // Name:                DoSomethingInteresting
        // Parameters:          int myNumber, ... string myString
        // Function Body:       { ... everything between the curly braces ... }
        // Return Statement:    return "A string";
        string DoSomethingInteresting(int myNumber, double myFracture, bool myBoolean, string myString)
        {
            Console.WriteLine(myNumber);
            Console.WriteLine(myFracture);
            Console.WriteLine(myBoolean);
            Console.WriteLine(myString);

            return "Hello there";
        }

        #endregion
    }
}
