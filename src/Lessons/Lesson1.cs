﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessons
{
    class Lesson1
    {
        public static void Run()
        {
            #region 1. Variables

            // 1. Variables
            // A line of code (a statement) ends with a semicolon in Java or similar langues ;
            // A variable is a storage location to put a value in.
            // A variable has a name that is understandable to humans.

            int firstNumber;        // Declaration
            firstNumber = 10;       // Assignment

            int secondNumber = 5;   // Shorthand declaration and assignment

            // 2. Basic Types
            string myName = "Chris";           // String: A sentence, a word, any range of non-numeric characters.
            bool isAProgrammer = true;         // Boolean: A value indicating if something is either TRUE or FALSE.
            int myAge = 23;                    // Integer: A number with no decimals (min: -2147483648, max: 2147483647)
            float mySavings = 231.50f;         // Float: A number with decimals.

            // More precision
            long aHugeNumber = 9223372036854775807L;  // Long: A number with no decimals which can store bigger numbers than int. (min: -9223372036854775808, max: 9223372036854775807)
            double applePie = 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513282306647093844609550582231725359408128481117450284102701938521105559644622948954930381964428810975665933446128475648233786783165271201909145648566923460348610454326648213393607260249141273724587006606315588174881520920962829254091715364367892590360011330530548820466521384146951941511609;
            // Double: A number with decimals and double the precision of a float (more decimal places).

            #endregion
            #region 2. Operators

            // 2. Operators
            // Operators change values, like in math.

            int a = 10;
            int b = 5;

            int aPlusB = a + b; // 15
            int aMinusB = a - b; // 5
            int aTimesB = a * b; // 50
            int aDividedByB = a / b; // 2

            // Modulo operator (gets a remainder)

            // For checking if one number can be devided by the other without getting a fracture.
            int remainderOfADividedByB = a % b; // 0, 

            // The number 5 can fit 2 times in twelve without a fracture. The remainder is 2.
            int c = 12;
            int d = 5;
            int remainderOfCDividedByD = c % d; // 2
            Console.WriteLine(remainderOfCDividedByD);
            // Also useful for checking if a number is even or odd:
            // If (lineNumber % 2) has a remainder of 0 the number is even, if it has a remainder of 1 the number is odd.

            // Shorthand assignment
            int x = 5;
            int y = 10;

            x = x + y;
            // Can also be written as:
            x += y;

            x = x - y;
            // Can also be written as:
            x -= y;

            x = x * y;
            // Can also be written as:
            x *= y;

            x = x / y;
            // Can also be written as:
            x /= y;
            //0.4 -> 0
            //0.5 -> 1

            x = x + 1;
            // Can also be written as:
            x++;

            x = x - 1;
            // Can also be written as:
            x--;

            #endregion
            #region 3. Conditional statements and comparison operators

            // 3. Control Structures and comparison operators
            // Control structures are blocks of code which
            // use variables and their values to decide what needs to happen

            #region 3.1 Simple condition

            // 2.1 Simple condition
            if (firstNumber > secondNumber)
            {
                // We get here if [firstNumber] is greater than [secondNumber]
                // If this is not the case, the code will ignore this block.
                Console.WriteLine("Super!");
            }

            if (firstNumber < secondNumber)
            {
                // We get here if [firstNumber] is less than [secondNumber]
                // If this is not the case, the code will ignore this block.
            }

            #endregion 
            #region 3.2 Multiple conditions

            // 3.2 Multiple conditions
            if (myName == "Sveta")
            {
                // We get here if [myName] is equal to "Sveta"
                Console.WriteLine("Hello there, Sveta");
            }
            else if (myAge == 30)
            {
                // We get here if [myName] is equal to "Christiaan"
                Console.WriteLine("Hello there, Chris");
                if (myAge > 90)
                {
                    Console.WriteLine("You are old!");
                }
            }
            else
            {
                // We get here if none of the above are TRUE
                Console.WriteLine("Neither Chris nor Sveta? Who the hell are you?");
            }

            #endregion
            #region 3.3 Comparison operators

            // 3.3 Comparison operators
            // >    Greater Than
            // <    Less Than
            // >=   Greater Than or Equal To
            // <=   Less Than or Equal To
            // ==   Equal To
            // !=   Not Equal to

            Console.Clear();

            int age = 18;
            int minimumDrivingAge = 18;
            if (age >= minimumDrivingAge)
            {
                Console.WriteLine("You are allowed to drive");
            }

            #endregion
            #region 3.4 Loops
            Console.Clear();

            // 3.4 Loops
            // Loops are blocks of code that keep repeating until a condition is met.
            int countA = 1;
            while (countA <= 10) // While the condition (countA <= 10) is not yet true, keep repeating the code.
            {
                // std::cout << "bla";
                // System.out.println("bla");
                Console.WriteLine("There are " + countA + " monkeys sitting in a tree.");
                countA++; // Make sure countA increases by 1
            }

            // Can also be written as:
            for (int countB = 1; countB <= 10; countB++)
            {
                Console.WriteLine("There are " + countB + " monkeys sitting in a tree.");
            }

            #endregion

            #endregion

            // Differences between if-, if-/elseif-/else- and nested if-statements

            // A. Seperate if-statements.
            // Both conditions will always be checked.
            if (myName == "Света")
            {
                Console.WriteLine("Hello there, Света");
            }

            if (myAge == 30)
            {
                Console.WriteLine("Hello there random 30 year old");
            }

            // B. if-/elseif-/else-statement
            // Only the code for the first matching condition will be executed
            if (myName == "Света")
            {
                Console.WriteLine("Hello there, Света");
            }
            else if (myAge == 30)
            {
                Console.WriteLine("Hello there random 30 year old. You can't be Света.");
            }

            // C. Nested if-statements
            if (myName == "Света")
            {
                Console.WriteLine("Hello there, Света");
            }
            else
            {
                // By using a nested if-statement, we have the room to execute some additional logic
                // This is not possible in the elseif above.
                Console.WriteLine("You are not Света, but let's check if you are 30");
                if (myAge == 30)
                {
                    Console.WriteLine("Hello there random 30 year old.");
                }
            }

            // 5² -> 5 to the power of 2 -> 5*5
            // 5³ -> 5 to the power of 2 -> 5*5*5
            int fiveToThePowerOfTwoByChris = RaiseToThePowerOf(5, 2);
            int another = RaiseToThePowerOf(10, 3);
            //string fiveToThePowerOfTwoByChrisString = RaiseToThePowerOf(5, 2); // Wrong! Return type is int
            double fiveToThePowerOfTree = Math.Pow(5, 2); // Built-in function doing the same.

            /*
            while (1 == 1) 
            {
                // Bad: Eternal loop.
            }

            while (true) 
            {
                // Eternal loop, but with a way to exit.
                if(someOtherThing == 10){    
                    break;
                }
            }
            
            buttonPushed = "";
            bool gameIsRunning = true;
            while(gameIsRunning)
            {
                // Do game things.
                if(buttonPushed == "ESC")
                {
                    gameIsRunning = false;
                }
            }
            */
        }


        // A function/method is a piece of reusable code
        // It has a name, input and output.
        // I could write this once, but use it as much as i want.

        // The public static part is not important.

        //  4³ -> number = 4, exponent = 3 -> 4*4*4
        // RaiseToThePowerOf(4, 3);
        /// <returns>number raised to the power of exponent</returns>
        public static int RaiseToThePowerOf(int number, int exponent)
        {
            for (int count = 1; count < exponent; count++)
            {
                number *= number;
            }
            return number;
        }
    }
}
