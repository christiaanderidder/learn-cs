﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessons
{
    class Lesson2
    {
        public static void Run()
        {
            // RECAP

            // Variables
            int aNumber = 10;
            float aFracture = 0.25f;
            string aWord = "Cats";
            bool isTheLightOn = true;

            // Operators
            int anotherNumber = aNumber * 10;
            float anotherFracture = aFracture / 5.5f;
            string aPhrase = "Look at my pictures of " + aWord + "!";
            string anotherPhrase = "The light is on: " + isTheLightOn;

            aNumber += 5;
            aFracture *= 10;
            aFracture = aFracture * 10;

            // If-statements
            Console.WriteLine("The word is: " + aWord);
            if (aWord == "Cats")
            {
                Console.WriteLine("STOP TALKING ABOUT CATS!!!");
            }
            else if (aWord == "Dogs")
            {
                Console.WriteLine("STOP TALKING ABOUT DOGS!!!");
            }
            else
            {
                Console.WriteLine("That's better!");
            }

            // Loops
            int theCount = 0;
            while(theCount < 10)
            {
                //Do Stuff
                theCount++; 
            }
            Console.WriteLine(theCount);

            for (int theNumber = 0; theNumber < 10; theNumber++)
            {
                // 10 / 3 = 3.333333333333
                // 10 % 3 = 1 (3 + 3 + 3 = 9 with a remainder of 1)

                if (theNumber % 2 == 0)
                {
                    Console.WriteLine("The number " + theNumber + " is even.");
                }
                else
                {
                    Console.WriteLine("The number " + theNumber + " is odd.");
                }

            }



            #region Complex conditions

            // Sometimes more complex conditions are required.
            // To prevent if-/elsif-/else-clauses from becoming too complex there are
            // several operators to check complex conditions.

            string country = "NL";
            int age = 16;

            if (country == "NL")
            {
                if (age >= 18)
                {
                    Console.WriteLine("You may drive a car in the Netherlands.");
                }
                else
                {
                    Console.WriteLine("You are too young to drive in the Netherlands.");
                }
            }
            else if (country == "US")
            {
                if (age >= 16)
                {
                    Console.WriteLine("You may drive a car in the USA.");
                }
                else
                {
                    Console.WriteLine("You are too young to drive in the USA.");
                }
            }
            else
            {
                Console.WriteLine("There is no data for your country.");
            }

            // We can make the code a bit shorter and more readable by combining the country
            // and age checks into one condition. To do so we need to use the special
            // AND operator '&&':
            if (country == "NL" && age >= 18)
            {
                Console.WriteLine("You may drive a car in the Netherlands.");
            }
            else if (country == "US" && age >= 16)
            {
                Console.WriteLine("You may drive a car in the USA");
            }
            else
            {
                Console.WriteLine("You are either too young or there is no data for your country.");
            }

            // Or to make it even shorter, using the OR operator '||':
            if ((country == "NL" && age >= 18) || (country == "US" && age >= 16))
            {
                Console.WriteLine("You may drive a car.");
            }
            else
            {
                Console.WriteLine("You are either too young or there is no data for your country.");
            }

            // Some programming languages use && and || (c, c#, java, php, etc.) others just use the words 'and' and 'or' (ruby, vbscript, python, etc.)

            // As you can see, shortening the conditions to one line made the if-statement a lot shorter.
            // However, shortening conditions using AND and OR did make us lose some
            // specific places to run code.

            // Conditions can also be used outside of if statements to assign Boolean variables:
            bool canDrive = ((country == "NL" && age >= 18) || (country == "US" && age >= 16));
            if (!canDrive) // Could also be written as (canDrive != true) or (canDrive == false)
            {
                Console.WriteLine("You are either too young or there is no data for your country.");
            }
            else
            {
                Console.WriteLine("You may drive a car.");
            }

            #endregion
            #region Arrays

            // Empty the output window.
            Console.Clear();

            // ARRAYS
            // Variables represent a single value, a number, a word, true/false, etc.
            // Arrays are used to store multiple values.

            #region TheWrongWay

            // Do not do this:
            string firstPerson = "Светочка";
            string secondPerson = "Christiaan de Ridder";
            string thirdPerson = "Александр Сергеевич Пушкин";
            string fourthPerson = "Rembrandt Harmensz. van Rijn";

            // Why not:
            // 1. The order is fixed in the variable name
            // 2. Managing a big list this way is a complex task
            // 3. What happens if we want to greet every person?
            Console.WriteLine("Hello, " + firstPerson);
            Console.WriteLine("Hello, " + secondPerson);
            Console.WriteLine("Hello, " + thirdPerson);
            Console.WriteLine("Hello, " + fourthPerson);

            // DRY - DON'T REPEAT YOURSELF

            #endregion

            Console.WriteLine("------------------------");

            #region TheRightWay

            // Instead use an array to store all the names.

            // Arrays
            // An Array is a fixed size list that can only contain items of one type.
            // Every item in an array gets its own unique index.
            // Arrays are zero-indexed. This means they start with 0, not with 1.

            // Declaring and assigning arrays works the same as initalizing variables, but is slightly different
            
            string hello = "hello";
            string[] someListOfNames = new string[4];
            int[] listOfNumbers = new int[50];
            // The of the variable 'names' is not 'string' but 'string[]' the bracked indicate that this
            // is an array of 'string'.

            // The 'new' keyword is always used when initializing data structures,
            // it tells the computer to reserve the memory for the data structure.
            // The variable 'numberOfNames' tells the computer for how many items it needs to reserve
            // memory. Logically a long list requires more memory than a short list.

            int numberOfNames = 4;
            string[] listOfNames = new string[numberOfNames];
            // Assigning values. Again, beware of zero-indexing.
            listOfNames[0] = "Светочка";
            listOfNames[1] = "Christiaan de Ridder";
            listOfNames[2] = "Александр Сергеевич Пушкин";
            listOfNames[3] = "Rembrandt Harmensz. van Rijn";
            //listOfNames[4] = "The fourth person??"; // WRONG! This would be the fifth person, we start couting at 0.

            Console.WriteLine(listOfNames[0]);
            

            // Now the awesome part.
            for (int i = 0; i < numberOfNames; i++)
            {
                Console.WriteLine("привет, " + listOfNames[i]);
            }

            int j = 0;
            while (j < numberOfNames)
            {
                Console.WriteLine("привет, " + listOfNames[j]);
                j++;
            }

            // Mistakes made often:
            /*
            for(int i = 0; i < numberOfNames; i++) {
                Console.WriteLine("Hello, " + names[i]);
            }
            */

            #endregion

            #endregion

            Console.ReadLine();
        }
    }
}
