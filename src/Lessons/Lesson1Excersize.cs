﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessons
{
    class Lesson1Excersize
    {
        public static void Run()
        {
            Console.WriteLine("What's your name?");
            string name = Console.ReadLine();

            if(name != "Chris")
            {
                Console.WriteLine("We need another guy here.");
                return;
            }

            Console.WriteLine("How old r u?");
            int age = int.Parse(Console.ReadLine());

            if(age >= 18)
            {
                Console.WriteLine("Yay!");
            }
            else
            {
                Console.WriteLine("Nay!");
            }

            Console.WriteLine("are we getting here?!");
        }
    }
}
